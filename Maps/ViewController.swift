//
//  ViewController.swift
//  Maps
//
//  Created by Students on 19/04/2018.
//  Copyright © 2018 Students. All rights reserved.
//

import UIKit
import MapKit
class ViewController: UIViewController {

    @IBOutlet weak var filtri: UIButton!
    @IBOutlet weak var karte: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var d : String = ""
         var n : String = ""
         var long : String = ""
         var lat : String = ""
        
        var d2 : String = ""
        var n2 : String = ""
        var long2 : String = ""
        var lat2 : String = ""
        
        var d3 : String = ""
        var n3 : String = ""
        var long3 : String = ""
        var lat3 : String = ""
    
        var d4 : String = ""
        var n4 : String = ""
        var long4 : String = ""
        var lat4 : String = ""

        if let path = Bundle.main.path(forResource: "Data", ofType: "plist"){
             let arr = NSArray(contentsOfFile: path)
               debugPrint(path)
            if let dict=arr{
                let value = dict[0] as! NSDictionary
                d = value ["des"] as! String
              
                n = value ["name"] as! String
                
                long = value ["longitude"] as! String
                
                lat = value ["latitude"] as! String
                
            }
            if let dict=arr{
                let value = dict[1] as! NSDictionary
                d2 = value ["des"] as! String
             
                n2 = value ["name"] as! String
     
                long2 = value ["longitude"] as! String
                
                lat2 = value ["latitude"] as! String
     
            }
            if let dict=arr{
                let value = dict[2] as! NSDictionary
                d3 = value ["des"] as! String
            
                n3 = value ["name"] as! String
              
                long3 = value ["longitude"] as! String
            
                lat3 = value ["latitude"] as! String
              
            }
            if let dict=arr{
                let value = dict[3] as! NSDictionary
                d4 = value ["des"] as! String
                
                n4 = value ["name"] as! String
                
                long4 = value ["longitude"] as! String
                
                lat4 = value ["latitude"] as! String
                
            }
        }
       
    
        let firstPoint = MKPointAnnotation()
        firstPoint.coordinate = CLLocationCoordinate2D(latitude: Double(lat)!, longitude: Double(long)!)
        firstPoint.title=n
        firstPoint.subtitle=d
        karte.addAnnotation(firstPoint)
        
        let secondPoint = MKPointAnnotation()
        secondPoint.coordinate = CLLocationCoordinate2D(latitude: Double(lat2)!, longitude: Double(long2)!)
        secondPoint.title=n2
        secondPoint.subtitle=d2
        karte.addAnnotation(secondPoint)
        
        let thirdPoint = MKPointAnnotation()
        thirdPoint.coordinate = CLLocationCoordinate2D(latitude: Double(lat3)!, longitude: Double(long3)!)
        thirdPoint.title=n3
        thirdPoint.subtitle=d3
        karte.addAnnotation(thirdPoint)
        
        let fourthPoint = MKPointAnnotation()
        fourthPoint.coordinate = CLLocationCoordinate2D(latitude: Double(lat4)!, longitude: Double(long4)!)
        fourthPoint.title=n4
       fourthPoint.subtitle=d4
        karte.addAnnotation(fourthPoint)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

