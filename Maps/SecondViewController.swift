//
//  SecondViewController.swift
//  Maps
//
//  Created by Students on 19/04/2018.
//  Copyright © 2018 Students. All rights reserved.
//

import UIKit
import MapKit
protocol SecondViewControllerDelegate{
    func textUpdated (newText: String)
}
class SecondViewController: UIViewController {
    
    @IBOutlet weak var toggle: UISwitch!
    
    @IBAction func toggle(_ sender: Any) {
        if((sender as AnyObject).isOn==true)
        {
            debugPrint("Ieslēgts")
        }
            
        else{
            debugPrint("Izslēgts")
        }
    }
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    



}
